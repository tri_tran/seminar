
ROOT FOLDER: web/

ACCOUNT:
 - Administrator: admin/ qazXSW@1
 - Normal user: user01/ 123456

PAGE:
 - Seminar list - Url: /seminar-list
 - Content type: Seminar

DATABASE:
 - File: seminar.sql

CONFIG database:
 - Path file: web/sites/default/settings.php (line 772)

CUSTOM MODULE:
 - Path folder: web/modules/custom/seminar_utils

CUSTOM THEME:
 - Path folder: web/themes/custom/seminar_theme

