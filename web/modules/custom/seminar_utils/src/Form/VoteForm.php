<?php

namespace Drupal\seminar_utils\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\comment\Entity\Comment;

/**
 * Build a custom form to Vote
 * Class VoteForm
 * @package Drupal\seminar_utils\Form
 */
class VoteForm extends FormBase
{

  /**
   * Defines the unique ID for the form
   * @return string
   */
  public function getFormId() {
    return 'seminar_vote_form';
  }


  /**
   * Form constructor.
   * Triggered when the user requests the form. Builds the basic $form array with all the fields of the form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $nid = NULL) {

    /**
     * Define custom template for form.
     * Template path: web/modules/custom/seminar_utils/templates/vote-form.html.twig
     */
    $form['#theme'] = 'vote_form';

    if (!is_null($nid)) {
      $node = Node::load($nid); // load seminar by id
      if ($node) {

        $vote_options = $node->get('field_seminar_vote_option')->getValue();
        if(!empty($vote_options)) {

          // Build checkbox vote options
          $total_voting = $node->get('field_seminar_total_voting')->getValue()[0]['value'];
          $vote_options = array_values($vote_options);
          $vote_options = array_column($vote_options, 'value');

          $form['vote_options'] = array(
            '#title' => 'Vote options:',
            '#type' => 'radios',
            '#default_value' => '0',
            '#options' => $vote_options,
            '#prefix' => "<h4>Total of voting: $total_voting</h4>"
          );

          $current_user_id = \Drupal::currentUser()->id();
          // Load vote log
          $vote_logs = \Drupal::entityTypeManager()
            ->getStorage('comment')
            ->loadByProperties([
              'uid' => $current_user_id,
              'entity_id' => $nid,
            ]);

          if (!empty($vote_logs)) {

            // Load vote option's value, which already checked by normal user
            $vote_log = reset($vote_logs);
            $vote_option = $vote_log->get('field_vote_option')->getValue()[0]['value'];
            $form['vote_options']['#default_value'] = array_search($vote_option ,$vote_options);
          }

          $form['seminar_id'] = [
            '#type' => 'hidden',
            '#value' => $nid
          ];
        }

        $form['actions'] = [
          '#type' => 'actions',
        ];

        // Add a submit button that handles the submission of the form.
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Submit'),
        ];
      }
    }
    return $form;
  }


  /**
   * Triggered when the form is submitted. It’s used to check the values collected by the form and optionally raise errors.
   * Validate vote option must be checked.
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    parent::validateForm($form, $form_state);
    $vote_option = $form_state->getValue('vote_options');
    if ($vote_option == '') {
      $form_state->setErrorByName('vote_options', $this->t('You must choose a option to continue'));
    }
  }


  /**
   * Form submission handler.
   * Used to carry out the form submission process, if the form has passed validation with no errors.
   * Update total vote. Save vote log
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {

    $seminar_id = $form['seminar_id']['#value'];
    if ($seminar_id) {

      // Update total vote
      $node = Node::load($seminar_id);
      $total_voting = $node->get('field_seminar_total_voting')->getValue()[0]['value'];
      $total_voting = (int)$total_voting + 1;
      $node->set('field_seminar_total_voting', $total_voting);
      $node->save();

      $key = $form_state->getValue('vote_options');
      $vote_options = isset($form['vote_options']['#options']) ? $form['vote_options']['#options'] : [];

      // Save vote log, including: normal user & vote option of user selected
      if ($value_voted = $vote_options[$key]) {

        $current_user_id = \Drupal::currentUser()->id();
        $vote_logs = \Drupal::entityTypeManager()
          ->getStorage('comment')
          ->loadByProperties([
            'uid' => $current_user_id,
            'entity_id' => $seminar_id,
            ]);

        if (!empty($vote_logs)) {
          // Update inform to vote log already exist
          $vote_log = reset($vote_logs);
          $vote_log->set('field_vote_option', $value_voted);
        }
        else {
          // Create new vote log
          $vote_log = Comment::create([
            // These values are for the entity that you're creating the comment for, not the comment itself.
            'entity_type' => 'node',            // required.
            'entity_id'   => $seminar_id,       // required.
            'field_name'  => 'field_seminar_vote_log',         // required.
            // The user id of the comment's 'author'. Use 0 for the anonymous user.
            'uid' => $current_user_id,                         // required.
            // These values are for the comment itself.
            'comment_type' => 'comment',        // required.
            'subject' => 'Vote log '.$node->getTitle(),  // required.
            'field_vote_option' => $value_voted,
            // Whether the comment is 'approved' or not.
            'status' => 1,                      // optional. Defaults to 0.
          ]);
        }
        $vote_log->save();

        $messenger = \Drupal::messenger();
        $messenger->addMessage('Thank you for your vote');
      }
    }
  }
}